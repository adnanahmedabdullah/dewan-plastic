<?php
include '../conndata.php';
$conn = connectToDatabase();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if (isset($_POST['email'])) {
    $email = $_POST['email'];
  } else {
    $email = '';
  }
  if (empty($email)) {
    $response = [
      "success" => false,
      "message" => "Enter your email and submit"
    ];
  } else {
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
      if (strlen($email) <= 100) {
        $checkSql = "SELECT email FROM dp_newsletter WHERE email = '$email'";
        $result = $conn->query($checkSql);
        if ($result->num_rows > 0) {
          $response = [
            'success' => false,
            'message' => 'Your email has been added'
          ];
        } else {
          $sql = "INSERT INTO dp_newsletter (email) VALUES ('$email')";
          if ($conn->query($sql) === TRUE) {
            $response = [
              'success' => true,
              'message' => 'Thanks, your email has been added.'
            ];
          } else {
            $response = [
              'success' => false,
              'message' => 'Sorry, your email could not be added due to some problem, please submit email again.'
            ];
          }
        }
      } else {
        $response = [
          "success" => false,
          "message" => "Email cannot exceed 100 characters."
        ];
      }
    } else {
      $response = [
        "success" => false,
        "message" => "The email is incorrect, try with a valid email."
      ];
    }
  }
} else {
  $response = [
    'success' => false,
    'message' => 'Invalid request'
  ];
}

header('Content-Type: application/json');
echo json_encode($response);