<?php
function verifyRecaptcha($response) {
  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $data = array(
      'secret' => '6LczHkcoAAAAAHGqxcoAXeBAo2JMES5xLHJp0X_E',
      'response' => $response
  );

  $options = array(
      'http' => array(
          'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
          'method' => 'POST',
          'content' => http_build_query($data)
      )
  );

  $context = stream_context_create($options);
  $result = file_get_contents($url, false, $context);
  $resp = json_decode($result);
  
  return $resp->success;
}

function messageTG($from, $to, $msg) {
  $telegramApiUrl = "https://api.telegram.org/bot{$from}/sendMessage";
  
  $telegramApiParams = array(
    'chat_id' => $to,
    'text' => $msg,
    'parse_mode' => 'HTML'
  );
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $telegramApiUrl);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $telegramApiParams);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($ch);
  curl_close($ch);
  
  return $response;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  $name = $_POST['name'];
  $number = $_POST['number'];
  $subject = $_POST['subject'];
  $message = $_POST['message'];
  $recaptcha = $_POST['g-recaptcha-response'];
  
  $telegramBotToken = '6839054765:AAGIgfTWHxfdsqx-kSkFKFsLPf87upVrbG0';
  
  $errors = array();
  if (empty($name)) {
    $errors['name'] = 'Name';
    $name = 'Not filled';
  }
  if (empty($number)) {
    $errors['number'] = 'Phone number';
    $email = 'Not filled';
  }
  if (empty($subject)) {
    $errors['subject'] = 'Subject';
    $subject = 'Not filled';
  }
  if (empty($message)) {
    $errors['message'] = 'Message';
    $message = 'Not filled';
  }
  if (empty($recaptcha)) {
    $errors['captcha'] = 'reCAPTCHA';
    $recaptcha = 'Not filled';
  }
  
  if (!empty($errors)) {
    $error_message = implode(', ', $errors) . ' must be filled';
    $chatID = '5967601798';
    $response = [
      'success' => false,
      'message' => $error_message
    ];
  } else {
    $chatID = '-4003544680';
  }
  
  if (verifyRecaptcha($recaptcha)) {
    $recaptchaFR = 'OK';
  } else {
    $recaptchaFR = 'Not OK';
  }
  
  if (!empty($telegramBotToken) && !empty($chatID)) {
    $telegramMessage = "<b>Name:</b> <code>" . $name . "</code>\n";
    $telegramMessage .= "<b>Phone Number:</b> <code>" . $number . "</code>\n";
    $telegramMessage .= "<b>Subject:</b> <code>" . $subject . "</code>\n";
    $telegramMessage .= "<b>Message:</b> <code>" . $message . "</code>\n";
    $telegramMessage .= "<b>reCAPTCHA:</b> <code>" . $recaptchaFR . "</code>";
    
    $res = messageTG($telegramBotToken, $chatID, $telegramMessage);
    
    if ($res->ok === false) {
      $response = [
        'success' => false,
        'message' => 'somethink wrong! please try again or contact us by email.'];
    } else {
      if (empty($errors)) {
        $response = [
          'success' => true,
          'message' => 'Thenks for contacting us, We will contact you as soon as possible.'
        ];
      }
    }
  }
} else {
  $response = [
    'success' => false,
    'message' => $_SERVER["REQUEST_METHOD"] . ' is a wrong method'];
}

header('Content-Type: application/json');
echo json_encode($response);