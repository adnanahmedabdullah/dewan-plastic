const contactForm = document.getElementById('contact-form');
const contactMsg = document.getElementById('contact-msg');
const newsletterForm = document.getElementById('newsletter-form');
const newsletterMsg = document.getElementById('newsletter-msg');

contactForm.addEventListener('submit', e => {
  e.preventDefault();
  
    const data = new FormData(contactForm);
  
  fetch('/api/contact/', {
    method: 'POST',
    body: data
  })
  .then(res => res.json())
  .then(res => {
    if(res.success) {
      showMsg(contactMsg, contactForm, res.message, false);
    } else {
      showMsg(contactMsg, contactForm, res.message, true);
    }
  })
  .catch(err => {
    showMsg(contactMsg, contactForm, err, true);
  })
});

newsletterForm.addEventListener('submit', e => {
  e.preventDefault();
  
  const formData = new FormData(newsletterForm);
  
  fetch('/api/newsletter/', {
    method: 'POST',
    body: formData
  })
  .then(res => res.json())
  .then(res => {
    if(res.success) {
      showMsg(newsletterMsg, newsletterForm, res.message, false);
    } else {
      showMsg(newsletterMsg, newsletterForm, res.message, true);
    }
  })
  .catch(err => {
    showMsg(newsletterMsg, newsletterForm, err, true);
  })
});

function showMsg(ele, eleF, msg, err) {
  if(err) {
    ele.style.color = 'red';
  } else {
    ele.style.color = 'green';
    eleF.reset();
  }
  ele.innerText = msg;
  ele.style.display = 'block'
  setTimeout(() => {
    ele.innerText = '';
    ele.style.display = 'none';
  }, 3000);
}